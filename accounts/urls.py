from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.index, name='home'),
    path('login/', views.login_page, name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('register/', views.register_page, name='register'),
    path('profile/', views.profile, name='profile'),
    path('update/', views.update_user, name='update'),
    path('delete/', views.delete_user, name='delete'),
    path('shop/', views.shop, name='shop'),
    path('product_info/<str:item>', views.product_page, name='product'),
    path('add_to_cart/<str:item>', views.add_to_cart, name='add_to_cart'),
    path('cart/', views.cart, name='cart'),
    path('clear_cart/<str:order_id>', views.clear_cart, name='clear_cart'),
    path('checkout/<str:order_id>', views.checkout, name='checkout'),
    path('cnf/<str:order_id>', views.confirmed, name='confirmed'),
    path('staff/', views.staff, name='staff'),
    path('confirm_order/<str:order_id>', views.confirm_order, name='confirm_order'),
    path('delete_order/<str:order_id>', views.delete_order, name='delete_order'),
    path('verify/', views.verify, name='verify'),
    path('forgot_password/', views.forgot_password, name='forgot_password'),
    path('reset_password/', views.reset_password, name='reset_password'),
    path('change_password_page/', views.change_password_page, name='change_password_page'),
    path('change_password/', views.change_password, name='change_password'),
]
