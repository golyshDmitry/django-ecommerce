import datetime
# from multiprocessing import Process
from django.contrib import admin
from django.core.mail import send_mail
from .models import CustomUser, Product, Order, OrderItem


VERIFY_URL = 'http://127.0.0.1:8000/verify/'


def send_letter(modeladmin, request, queryset):
    date = datetime.datetime.now()

    send_mail(
        'Verification mail',
        f'Hello, {queryset[0].username}! Click this link to verify your account: {VERIFY_URL}. Mail sended: {date}',
        'dmitrij.golysh@gmail.com',
        [f'{queryset[0].email}'],
        fail_silently=False
    )

    # proc = Process(target=send_mail(
    #         'Verification mail',
    #         f'Hello, {queryset[0].username}! Click this link to verify your account: {VERIFY_URL}. Mail sended: {date}',
    #         'dmitrij.golysh@gmail.com',
    #         [f'{queryset[0].email}'],
    #         fail_silently=False
    #     )
    # )
    #
    # proc.start()
    # proc.join()


send_letter.short_description = 'Sending mails'


class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['username', 'verified']
    # ordering = ['user']
    actions = [send_letter]


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderItem)
