import datetime
import logging
from django.shortcuts import render, redirect, get_object_or_404
from django.core.mail import send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import CreateCustomUserForm, ChangeCustomUserForm, CustomUserInfoForm
from .models import Product, Order, OrderItem, Transaction, CustomUser


RESET_URL = 'http://127.0.0.1:8000/change_password_page/'

info_logger = logging.getLogger('authentication_logger')
warning_logger = logging.getLogger('account_logger')


def index(request):
    context = {'products': Product.objects.all()}
    return render(request, 'accounts/index.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            info_logger.info(f'{username} logged in his account.')
            return redirect('home')
        else:
            messages.info(request, 'Username or password is incorrect!')
            warning_logger.warning('Username or password is incorrect!')

    return render(request, 'accounts/login.html')


def register_page(request):
    if request.user.is_authenticated:
        return redirect('home')

    form = CreateCustomUserForm()

    if request.method == 'POST':
        form = CreateCustomUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account was successfully created for {username}')
            info_logger.info(f'Account was successfully created for {username}')

            return redirect('login')

    context = {'form': form}
    return render(request, 'accounts/register.html', context)


def logout_user(request):
    info_logger.info(f'{request.user} logged out of his account.')
    logout(request)
    return redirect('home')


def profile(request):
    if not request.user.is_authenticated:
        return redirect('home')

    form = CustomUserInfoForm(instance=request.user)
    context = {'form': form}

    return render(request, 'accounts/profile.html', context)


def update_user(request):
    if not request.user.is_authenticated:
        return redirect('home')

    form = ChangeCustomUserForm(instance=request.user)

    if request.method == 'POST':
        form = ChangeCustomUserForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile')

    context = {'form': form}
    return render(request, 'accounts/update_user.html', context)


def delete_user(request):
    if not request.user.is_authenticated:
        return redirect('home')

    user = request.user

    if request.method == 'POST':
        user.delete()
        return redirect('home')

    return render(request, 'accounts/delete_user.html')


def product_page(request, item):
    prod = Product.objects.get(id=item)
    context = {'prod': prod}

    return render(request, 'accounts/single-product.html', context)


def shop(request):
    item, sorting, page = request.GET.get('search'), request.GET.get('sorting'), request.GET.get('page', 1)
    # sorting = request.GET.get('sorting')

    if item:
        items = Product.objects.filter(name__icontains=item)
    else:
        items = Product.objects.all()

    if sorting == 'ascending':
        items = sorted(items, key=lambda i: i.price)
    elif sorting == 'descending':
        items = sorted(items, key=lambda i: i.price, reverse=True)

    paginator = Paginator(items, 6)

    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    context = {'items': items, 'sorting': sorting}

    return render(request, 'accounts/shop.html', context)


def get_user_pending_order(request):
    # get order for the correct user
    user_profile = get_object_or_404(CustomUser, username=request.user)
    order = Order.objects.filter(owner=user_profile, is_ordered=False)
    if order.exists():
        # get the only order in the list of filtered orders
        return order[0]
    return 0


def add_to_cart(request, item):
    if not request.user.is_authenticated:
        return redirect('login')

    product = Product.objects.get(id=item)

    order_item, status = OrderItem.objects.get_or_create(product=product)
    user_order, status = Order.objects.get_or_create(owner=request.user, is_ordered=False)
    user_order.items.add(order_item)

    return redirect('shop')

# def delete_from_cart(request, item):
#     item_to_delete = OrderItem.objects.get(id=item)
#     item_to_delete.delete()


def cart(request):
    if not request.user.is_authenticated:
        return redirect('login')

    existing_order = get_user_pending_order(request)
    context = {
        'order': existing_order
    }

    return render(request, 'accounts/cart.html', context)


def clear_cart(request, order_id):
    order = Order.objects.get(id=order_id)
    order.delete()

    return redirect('shop')


def checkout(request, order_id):
    order = Order.objects.get(id=order_id)
    context = {'order': order}

    return render(request, 'accounts/checkout.html', context)


def confirmed(request, order_id):
    order = Order.objects.get(id=order_id)
    order.is_ordered = True
    order.save()

    return redirect('home')


def staff(request):
    if request.user.is_staff is False:
        return redirect('home')

    orders = Order.objects.filter(is_ordered=True)
    context = {'orders': orders}

    return render(request, 'accounts/staff.html', context)


def delete_order(request, order_id):
    order = Order.objects.get(id=order_id)
    order.delete()

    return redirect('staff')


def confirm_order(request, order_id):
    order = Order.objects.get(id=order_id)
    order.status = 'Confirmed'
    order.save()

    return redirect('staff')


def verify(request):
    request.user.verified = True
    request.user.save()

    return redirect('home')


def forgot_password(request):
    return render(request, 'accounts/forgot_password.html')


def reset_password(request):
    email = request.GET.get('email')
    user = CustomUser.objects.get(email=email)

    send_mail(
        'Verification mail',
        f'Hello, {user.username}! Click this link to reset your password: {RESET_URL}. Mail sended: {datetime.datetime.now()}',
        'dmitrij.golysh@gmail.com',
        [f'{user.email}'],
        fail_silently=False
    )

    return redirect('home')


def change_password_page(request):
    return render(request, 'accounts/change_password.html')


def change_password(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = CustomUser.objects.get(username=username)

    user.set_password(password)
    user.save()

    return redirect('login')
