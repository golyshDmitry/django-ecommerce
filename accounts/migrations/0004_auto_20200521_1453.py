# Generated by Django 3.0.6 on 2020-05-21 14:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_order_orderitem_transaction'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='birth_date',
        ),
        migrations.AddField(
            model_name='customuser',
            name='phone',
            field=models.CharField(default=375292028482, max_length=32),
            preserve_default=False,
        ),
    ]
